package controllers

import play.api._
import play.api.libs.json.{Json}
import play.api.mvc._
import scala.concurrent.{Future}
import services._
import models.DribbleTO._
import scala.concurrent.ExecutionContext.Implicits.global

class Application extends Controller {

	val CLIENT_ACCESS_TOKEN = "8c415be78e410e11356df16f008ed0b6945a3667dbf6ccb2ecc8a3f787564ed3"

	def topTen(username: String) = Action.async { request =>
		DribbleAPI(CLIENT_ACCESS_TOKEN).getTopN(username, 10).map { top => 
			Ok(Json.toJson(top))
		}
	}
}
