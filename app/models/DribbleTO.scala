package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

object DribbleTO {

	case class User(
		username: String,
		followersCount: Option[Int] = None, 
		shotsCount: Option[Int] = None,
		likes:Option[Int] = None)

	object User {
		val userReads: Reads[User] = (
			(JsPath \ "username").read[String] and
			(JsPath \ "followers_count").readNullable[Int] and 
			(JsPath \ "shots_count").readNullable[Int] and 
			(JsPath \ "likes").readNullable[Int]
		)(User.apply _)

		val userWrites: Writes[User] = (
			(JsPath \ "username").write[String] and
			(JsPath \ "followers_count").writeNullable[Int] and
			(JsPath \ "shots_count").writeNullable[Int] and
			(JsPath \ "likes").writeNullable[Int]
		)(unlift(User.unapply))

		implicit val userFrmt: Format[User] = 
			Format(userReads, userWrites)
	}

	case class Follower(follower: User)
	
	object Follower {
		implicit val followerItemFrmt = Json.format[Follower]
	}

	case class Like(user: User)

	object Like {
		implicit val likeFrmt = Json.format[Like]
	}

	case class Shot(
		id: Int,
		likesCount: Option[Int] = None)

	object Shot {
		val shotReads: Reads[Shot] = (
			(JsPath \ "id").read[Int] and 
			(JsPath \ "likes_count").readNullable[Int]
		)(Shot.apply _)

		val shotWrites: Writes[Shot] = (
			(JsPath \ "id").write[Int] and
			(JsPath \ "likes_count").writeNullable[Int] 
		)(unlift(Shot.unapply))

		implicit val shotFrmt: Format[Shot] = 
			Format(shotReads, shotWrites)
	}

	case class ApiMessage(message: String)

	object ApiMessage {
		implicit val apiMessageFrmt = Json.format[ApiMessage]
	}
}