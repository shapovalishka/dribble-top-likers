package services

import play.api.libs.ws._
import scala.concurrent.{Await, Future, ExecutionContext}
import play.api.libs.json._
import scala.concurrent.duration._
import scalaz._
import Scalaz._
import scala.util.{Success, Failure}

import models.DribbleTO._

import play.api.Play.current

import akka.actor.ActorSystem
import akka.stream.scaladsl._
import akka.stream._

case class DribbleAPI(clientAccessToken: String) {
	val AUTHORIZATION = "Authorization"
	val AUTHORIZATION_VALUE = s"Bearer $clientAccessToken"

	val TOP_URL = "https://api.dribbble.com"
	val V1_USER  = (username: String) =>
		s"/v1/users/$username"
	val V1_USER_FOLLOWERS = (username: String) =>
		s"/v1/users/$username/followers"
	val V1_USER_SHOTS = (username: String) =>
		s"/v1/users/$username/shots"
	val V1_SHOT_LIKES = (id: Long) =>
		s"/v1/shots/$id/likes"


	//allows 1 request per second
	val dribbleAPIRate = 1001 millis
	val PER_PAGE = 100

	def printlnW(s: Any): Unit = println(Console.YELLOW + "WARNING: " + s + Console.RESET)
	def printlnE(s: Any): Unit = println(Console.RED + "ERROR: " + s + Console.RESET)

	def ws(url:String):Future[WSResponse] = {
		println(TOP_URL + url)
		WS.url(TOP_URL + url)
			.withHeaders(AUTHORIZATION -> AUTHORIZATION_VALUE)
			.get
	}

	def withFallback[T](f: => Future[T], onFail: T, n: Int = 3)(implicit ec: ExecutionContext):Future[T] = 
		f.recover {
			case err:Exception => 
				printlnE(s"Future failed with: ${err.getMessage()}")
				onFail
		}  	

	def wrapValidateAsFallable[T](response: WSResponse)(implicit frmt: Format[T]):T = 
		response.json.validate[T] match {
			case JsSuccess(result, _) => result
			case _ =>
				val msg = s"Dribble API failed with response: ${response.body}"
				throw new Exception(msg)
		}

	def fallbackUrl[T](url: String)(implicit frmt: Format[T]):Future[List[T]] = 
		withFallback(ws(url).map { response =>
			wrapValidateAsFallable[List[T]](response)
		}, List.empty[T])

	def pageWithFallback[T](url: String, page: Int, perPage:Int = PER_PAGE)(implicit frmt: Format[T]):Future[List[T]] =
			fallbackUrl[T](url + s"?page=$page&per_page=$perPage")

 	type UserPage = Tuple2[String, Int] // username -> page_number	
 	type ShotPage = Tuple2[String, Int] // username -> page_number
 	type LikePage = Tuple2[Int, Int] // shot_id -> page_number

 	def followersPages(username:String) :Future[List[UserPage]] = 
 		withFallback(ws(V1_USER(username)).map { response =>
			wrapValidateAsFallable[User](response)
		}, User(username = username, followersCount = Some(1))) map { user =>
 			getFollowerPages(user)
		}

	def getFollowerPages(user: User, perPage:Int = PER_PAGE): List[UserPage] = {
		val count = user.followersCount.getOrElse(1)
		val pages:Int = scala.math.floor(count / perPage).toInt + 1
		// println("followers PAGES" + pages)
		(1 to pages).map { p => 
			val up: UserPage = (user.username, p)
			up
		}.toList
	}

	def getShotPages(user: User, perPage:Int = PER_PAGE): List[ShotPage] =  {
		val count = user.shotsCount.getOrElse(1)
		val pages:Int = scala.math.floor(count / perPage).toInt + 1
		// println("SHOT PAGES" + pages)
		(1 to pages).map { p => 
			val up: ShotPage = (user.username, p)
			up
		}.toList
	}

	def getLikePages(shot: Shot, perPage:Int = PER_PAGE): List[LikePage] = {
		val count = shot.likesCount.getOrElse(1)
		val pages:Int = scala.math.floor(count / perPage).toInt + 1
		// println("Like PAGES" + pages)
		(1 to pages).map { p => 
			val up: LikePage = (shot.id, p)
			up
		}.toList
	}


	def followers(userPage: UserPage)(implicit ec: ExecutionContext):Future[List[User]] =  
		pageWithFallback[Follower](V1_USER_FOLLOWERS(userPage._1), userPage._2).map { followers =>
			followers.map(item => item.follower)
		}

	def shots(shotsPage: ShotPage)(implicit ec: ExecutionContext):Future[List[Shot]] = 
		pageWithFallback[Shot](V1_USER_SHOTS(shotsPage._1), shotsPage._2)

	def likers(likesPage: LikePage)(implicit ec: ExecutionContext):Future[List[User]] ={
		pageWithFallback[Like](V1_SHOT_LIKES(likesPage._1), likesPage._2).map { likers =>
			likers.map(item => item.user)
		} 
	}

	/**
	builds the following stream-processing graph.
	+------------+
	| tickSource +-Unit-+
	+------------+      +---> +-----+            +-----+      +-----+
	                      | zip +-(T,Unit)-> | map +--T-> | out |
	+----+              +---> +-----+            +-----+      +-----+
	| in +----T---------+
	+----+
	tickSource emits one element per `rate` time units and zip only emits when an element is present from its left and right
	input stream, so the resulting stream can never emit more than 1 element per `rate` time units.
	*/
	def throttle[T](rate: FiniteDuration): Flow[T, T, Unit] = {
		Flow() { implicit builder =>
			import akka.stream.scaladsl.FlowGraph.Implicits._
			val zip = builder.add(akka.stream.scaladsl.Zip[T, Unit.type]())
			akka.stream.scaladsl.Source(rate, rate, Unit) ~> zip.in1
			(zip.in0, zip.out)
		}.map(_._1)
	}


	val fetchFollowerPages: Flow[String, UserPage, Unit] =
		Flow[String]
			.via(throttle(dribbleAPIRate))
			.mapAsyncUnordered( username => followersPages(username) )
			.mapConcat(identity)

	val fetchShotPages: Flow[UserPage, ShotPage, Unit] =
	    Flow[UserPage]
	        .via(throttle(dribbleAPIRate))
	        .mapAsyncUnordered( followersPage => followers(followersPage) )
	        .mapConcat(identity)
	        .mapConcat(follower => getShotPages(follower))

	val fetchLikePages: Flow[ShotPage, LikePage, Unit] =
	 	Flow[ShotPage]
	 		.via(throttle(dribbleAPIRate))
	        .mapAsyncUnordered( shotsPage => shots(shotsPage) )
	        .mapConcat(identity)
	        .mapConcat(shot => getLikePages(shot))


	val fetchLikerUsernames: Flow[LikePage, User, Unit] =
	 	Flow[LikePage]
	 		.via(throttle(dribbleAPIRate))
	        .mapAsyncUnordered( likesPage => likers(likesPage) )
	        .mapConcat(identity)


	val usernameCountSink: akka.stream.scaladsl.Sink[User, Future[Map[String, Int]]] =
	    akka.stream.scaladsl.Sink.fold(Map.empty[String, Int])(
	      (acc: Map[String, Int], u: User) => 
	       acc |+| Map(u.username -> 1)
	    )

	implicit val as = ActorSystem()
	implicit val ec = as.dispatcher
	val settings = ActorFlowMaterializerSettings(as)
	implicit val mat = ActorFlowMaterializer(settings)


	def getTopN(username: String, n: Int)(implicit ec: ExecutionContext):Future[List[User]] = {
		val usernameSource: akka.stream.scaladsl.Source[String, Unit] =
			akka.stream.scaladsl.Source(List(username))

		val res: Future[Map[String, Int]] =
			usernameSource
				.via(fetchFollowerPages)
				.via(fetchShotPages)
				.via(fetchLikePages)
				.via(fetchLikerUsernames)
				.runWith(usernameCountSink)
		res.map(
			_.toList
			.sortWith(_._2 > _._2)
			.take(n)
			.map { case (username, likes) =>
					User(username, Some(likes))
				})
  	}


}